import { EventEmitter, Injectable } from "@angular/core";
import { MessageModel } from "../models/message.model";

@Injectable({
  providedIn: 'root',
})
export class MessageService {
  user1Message = new EventEmitter<MessageModel>();
  user2Message = new EventEmitter<MessageModel>();
}
