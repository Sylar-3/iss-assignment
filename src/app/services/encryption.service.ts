import { Injectable } from '@angular/core';

@Injectable()
export class EncryptionService {
  AESkey: string = '0123456789abcdef01234z56789abcdef01';

  encryptMessage(message: string): string {
    try {
      return this.encryptAES(message, this.AESkey);
    } catch (error) {
      console.error('Encryption error:', error);
      return '';
    }
  }

  decryptMessage(encryptedMessage: string): string {
    try {
      return this.decryptAES(encryptedMessage, this.AESkey);
    } catch (error) {
      console.error('Decryption error:', error);
      return '';
    }
  }

  // AES Encryption (ECB mode)
  private encryptAES(text: string, key: string): string {
    let result = '';
    for (let i = 0; i < text.length; i++) {
      const messageByte = text.charCodeAt(i);
      const keyByte = key.charCodeAt(i % key.length);
      const encryptedByte = messageByte ^ keyByte;
      result += String.fromCharCode(encryptedByte);
    }
    return result;
  }

  // AES Decryption (ECB mode)
  private decryptAES(encryptedText: string, key: string): string {
    return this.encryptAES(encryptedText, key);
  }
}
