import { EventEmitter, Injectable } from "@angular/core";
import { MessageModel } from "../models/message.model";

@Injectable({
  providedIn: 'root',
})
export class DisplayService {
  lastMessage = new EventEmitter<MessageModel>();
  encryptedMessage = new EventEmitter<string>();
}
