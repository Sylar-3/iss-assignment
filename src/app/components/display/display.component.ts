import { NgIf } from '@angular/common';
import { Component, WritableSignal, signal } from '@angular/core';
import { MessageModel } from 'src/app/models/message.model';
import { RadioButtonModule } from 'primeng/radiobutton';
import { DisplayService } from 'src/app/services/display.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@Component({
  selector: 'display-component',
  templateUrl: './display.component.html',
  styleUrls: ['./display.component.scss'],
  standalone: true,
  imports: [NgIf, RadioButtonModule, ReactiveFormsModule, FormsModule],
})
export class DisplayComponent {
  encryptedMessage: WritableSignal<string | null> = signal(null);
  lastMessage: WritableSignal<MessageModel | null> = signal(null);

  constructor(private _displayService: DisplayService) {
    this._displayService.lastMessage.subscribe((lastMessage: MessageModel) => {
      this.lastMessage.set(lastMessage);
    });

    this._displayService.encryptedMessage.subscribe(
      (encryptedMessage: string) => {
        this.encryptedMessage.set(encryptedMessage);
      }
    );
  }
}
