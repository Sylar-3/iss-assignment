import { Component } from '@angular/core';
import { UserOneComponent } from '../user1/user1.component';
import { UserTwoComponent } from '../user2/user2.component';
import { DisplayComponent } from '../display/display.component';

@Component({
  selector: 'chat-component',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.scss'],
  standalone: true,
  imports: [UserOneComponent, UserTwoComponent, DisplayComponent],
})
export class ChatComponent {}
