import { Component, WritableSignal, signal } from '@angular/core';
import { MessageModel } from 'src/app/models/message.model';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { FormControl, ReactiveFormsModule } from '@angular/forms';
import { NgClass, NgFor } from '@angular/common';
import { MessageService } from 'src/app/services/messaging.service';
import { DisplayService } from 'src/app/services/display.service';
import { EncryptionService } from 'src/app/services/encryption.service';

@Component({
  selector: 'user-two',
  templateUrl: './user2.component.html',
  styleUrls: ['./user2.component.scss'],
  standalone: true,
  imports: [InputTextareaModule, ReactiveFormsModule, NgFor, NgClass],
  providers: [EncryptionService],
})
export class UserTwoComponent {
  textAreaControl: FormControl<string | null> = new FormControl('');
  messageHistory: WritableSignal<Array<MessageModel>> = signal([]);

  constructor(
    private _messageService: MessageService,
    private _displayService: DisplayService,
    private _encService: EncryptionService
  ) {
    this._messageService.user1Message.subscribe((message) =>
      this.messageHistory.update((messagesList) => [
        ...messagesList,
        {
          user: message.user,
          message: this._encService.decryptMessage(message.message),
        },
      ])
    );
  }

  public sendMessage(): void {
    if (this.textAreaControl.value !== '') {
      //Same message here
      this.messageHistory.update((messagesList) => [
        ...messagesList,
        { user: false, message: this.textAreaControl.value ?? '' },
      ]);
      this._displayService.lastMessage.emit({
        user: false,
        message: this.textAreaControl.value ?? '',
      });

      //Encrypt here
      const encryptedMessage: string = this.encryptMessage();
      this._messageService.user2Message.emit({
        user: false,
        message: encryptedMessage,
      });
      this._displayService.encryptedMessage.emit(encryptedMessage);

      this.textAreaControl.setValue('');
    }
  }

  private encryptMessage(): string {
    return this._encService.encryptMessage(this.textAreaControl.value ?? '');
  }
}
