import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'chat',
    pathMatch: 'full',
    loadComponent: () =>
      import('./components/chat/chat.component').then(
        (component) => component.ChatComponent
      ),
  },
  {
    path: '**',
    pathMatch: 'full',
    redirectTo: 'chat',
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
